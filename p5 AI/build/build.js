var gui = new dat.GUI();
var params = {
    Download_Image: function () { return save(); },
};
gui.add(params, "Download_Image");
var models = new rw.HostedModel({
    url: "https://openpifpaf-pose-ef6d9f95.hosted-models.runwayml.cloud/v1/",
    token: "L6hrlD25iBWFLf0uV68s/A==",
});
var tmpImage;
var outImage;
var imag;
function draw() {
    if (imag)
        image(imag, 0, 0, width, height);
}
function preload() {
    tmpImage = loadImage("/Desktop/IMAC/IMAC 1/Algorithm Aestetic/Projet S2/Please me/Please me 1.png ");
}
function setup() {
    p6_CreateCanvas();
    var ia = new rw.HostedModel({
        url: "https://openpifpaf-pose-ef6d9f95.hosted-models.runwayml.cloud/v1/",
        token: "L6hrlD25iBWFLf0uV68s/A==",
    });
    tmpImage.loadPixels();
    var base64Image = tmpImage.canvas.toDataURL();
    var inputs = {
        "Image": base64Image,
    };
}
function windowResized() {
    p6_ResizeCanvas();
}
var __ASPECT_RATIO = 1;
var __MARGIN_SIZE = 25;
function __desiredCanvasWidth() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return windowWidth - __MARGIN_SIZE * 2;
    }
    else {
        return __desiredCanvasHeight() * __ASPECT_RATIO;
    }
}
function __desiredCanvasHeight() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return __desiredCanvasWidth() / __ASPECT_RATIO;
    }
    else {
        return windowHeight - __MARGIN_SIZE * 2;
    }
}
var __canvas;
function __centerCanvas() {
    __canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
}
function p6_CreateCanvas() {
    __canvas = createCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
function p6_ResizeCanvas() {
    resizeCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
var p6_SaveImageSequence = function (durationInFrames, fileExtension) {
    if (frameCount <= durationInFrames) {
        noLoop();
        var filename_1 = nf(frameCount - 1, ceil(log(durationInFrames) / log(10)));
        var mimeType = (function () {
            switch (fileExtension) {
                case 'png':
                    return 'image/png';
                case 'jpeg':
                case 'jpg':
                    return 'image/jpeg';
            }
        })();
        __canvas.elt.toBlob(function (blob) {
            p5.prototype.downloadFile(blob, filename_1, fileExtension);
            setTimeout(function () { return loop(); }, 100);
        }, mimeType);
    }
};
//# sourceMappingURL=../src/src/build.js.map